# RSPeche

Ce projet contient les sources du produit RSPeche.
Se référer au README de chaque projet pour avoir le détails les concernant. 

## Pour commencer

### Prérequis

Les projets s'appuient sur Spring boot pour la partie Back End

### Installation

L'installation de chaque composant est indépendante.

## Excécuter les tests

Les tests s'appuient essentiellement sur SpringRunner et H2 pour la persistance des données.

## Déploiment

Le déploiement s'effectue depuis des taches maven. 

### Accés au service : 

| Application                   |     Url                     |   commentaire   |
| ----------------------------- |:-------------------------   | :-------------- |
| rsp-admin-server              | <http://localhost:8101/>    |                 |
| rsp-security-oauth2           | <http://localhost:8100/>    |                 |
| rsp-serviceregistry-eureka    | <http://localhost:8761/>    |                 |
| rsp-picture-manager           | <http://localhost:8200/>    |                 |
| rsp-view-js                   |                             |                 |
|                               |                             |                 |


## Technologies

* [Spring boot](https://projects.spring.io/spring-boot/) - Frame
* [Maven](https://maven.apache.org/) - Manager de dépendance java
* [Docker](https://www.docker.com/) - Container

## Version

RSPeche utilise bitbucket (https://bitbucket.org/) pour la gestion de ses sources et de ces versions. Les versions sont visibles sous tags sur le répository https://pierre3313@bitbucket.org/pierre3313/rspeche.git

## Auteur

* **Pierre-Edouard Soupault** - *Version initiale*

## Copyright 

Ce programme est la propriété de Pierre-Edouard Soupault.