# RSP

Projet Parent RSPeche

## Pour commencer
https://github.com/joshiste/spring-boot-admin-samples
https://github.com/eugenp/tutorials/tree/master/spring-boot-admin
https://github.com/codecentric/spring-boot-admin
https://github.com/codecentric/spring-boot-admin/blob/master/spring-boot-admin-docs/src/main/asciidoc/security.adoc

https://stackoverflow.com/questions/46759710/build-docker-image-in-jenkins-in-docker-container-pipeline

### Prérequis

## Docker

Build image depuis plugin maven spotify

```
export DOCKER_HOST="tcp://vps550726.ovh.net:2375"
mvn clean package dockerfile:build 
```

Démarrer un container depuis l'image

docker run -p 8101:8101 --name=rsp-admin-server -d rsp/rsp-admin-server:1.0.0
docker run -p 8101:8101 --name=rsp-admin-server -d rsp/rsp-admin-server:latest

Création du réseau bridge
docker network create --driver bridge rsp-network
docker network connect rsp-network rsp-admin-server

## Technologies

* [Spring boot](https://projects.spring.io/spring-boot/) - Frame
* [Maven](https://maven.apache.org/) - Manager de dépendance java
* [Docker](https://www.docker.com/) - Container

## Version

RSPeche utilise bitbucket (https://bitbucket.org/) pour la gestion de ses sources et de ces versions. Les versions sont visibles sous tags sur le répository https://pierre3313@bitbucket.org/pierre3313/rspeche.git

## Auteur

* **Pierre-Edouard Soupault** - *Version initiale*

## Copyright 

Ce programme est la propriété de Pierre-Edouard Soupault.