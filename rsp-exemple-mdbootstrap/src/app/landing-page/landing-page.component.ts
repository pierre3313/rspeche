import { Component, OnInit } from '@angular/core';


import OlMap from 'ol/map';
import OlXYZ from 'ol/source/xyz';
import OlTileLayer from 'ol/layer/tile';
import OlVectorLayer from 'ol/layer/vector';
import OlVectorSource from 'ol/source/vector';
import OlView from 'ol/view';
import OlProj from 'ol/proj';
import OlFeature from 'ol/feature';
import OlPoint from 'ol/geom/point';
import Overlay from 'ol/overlay';
import OlStyle from 'ol/style/style';
import OlIcon from 'ol/style/icon';
import OlStroke from 'ol/style/stroke';
import OlCircleStyle from 'ol/style/circle';
import OlFill from 'ol/style/fill';
import OlInteraction from 'ol/interaction/interaction';
import { interaction } from '../../../node_modules/@types/openlayers';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  map: OlMap;
  source: OlXYZ;
  layer: OlTileLayer;
  view: OlView;
  feature: OlFeature;
  vectorLayer: OlVectorLayer;
  clickCoord: String;
  
  constructor() { }

  ngOnInit() {

    this.feature = new OlFeature({
      type: 'icon',
      geometry: new OlPoint(OlProj.fromLonLat([-0.5800364,44.841225])) // dont worry about coordinate type 0,0 will be in west coast of africa
    });     

    var styles = {
      'route': new OlStyle({
        stroke: new OlStroke({
          width: 6, color: [237, 212, 0, 0.8]
        })
      }),      
      'icon': new OlStyle({
        image: new OlIcon({
          anchor: [0.5, 1],
          src: 'assets/gps_PNG18.png'
        })
      }),
      'geoMarker': new OlStyle({
        image: new OlCircleStyle({
          radius: 7,
          snapToPixel: false,
          fill: new OlFill({color: 'black'}),
          stroke: new OlStroke({
            color: 'white', width: 2
          })
        })
      })
    };

    this.vectorLayer = new OlVectorLayer({
      source: new OlVectorSource({
        features: [this.feature]
      }),
      style: styles['icon']
    })

    //source dispo sur http://leaflet-extras.github.io/leaflet-providers/preview/
    this.source = new OlXYZ({
      url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
      //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'
    });

    this.layer = new OlTileLayer({
      source: this.source
    });

    this.view = new OlView({
      center: OlProj.fromLonLat([-0.5800364,44.841225]),
      zoom: 7
    });

    this.map = new OlMap({
      target: 'map',
      layers: [this.layer, this.vectorLayer],
      view: this.view
    });

    //Positionnement d'un marker sur la carte
    var lonLat = [-0.5800364,44.841225];
    this.setOverlay(lonLat);  

    var popup = new Overlay({
      element: document.getElementById('popup')
    });
    this.map.addOverlay(popup);

    this.map.on('click', evt => {
      this.setOverlayOnClick();
    });   
  }

  setOverlayOnClick(){
    var coord = this.map.getEventCoordinate(event);
    //var coord = evt.coordinate;
    var lonlat = OlProj.transform(coord, 'EPSG:3857', 'EPSG:4326');
    this.setOverlay(lonlat);
  }

  //Positionnement du marker
  setOverlay(lonLat){
    var pos = OlProj.fromLonLat(lonLat);
    this.clickCoord = lonLat[0] + ' ' + lonLat[1];
    var marker = new Overlay({
      position: pos,
      positioning: 'center-center',
      element: document.getElementById('marker'),
      stopEvent: false
    });

    this.map.addOverlay(marker);
  }
}
