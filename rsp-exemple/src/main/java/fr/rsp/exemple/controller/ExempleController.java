package fr.rsp.exemple.controller;

import fr.rsp.exemple.model.UserExemple;
import org.springframework.security.core.Authentication;

import java.security.Principal;


public interface ExempleController{

    public UserExemple getSimpleUserExemple();

    public UserExemple getSimpleUserExempleWithRoleAdmin();

    public UserExemple getSimpleUserExempleWithScope();

    public UserExemple getUserExempleById(Integer id);

    public Principal getUserAuthentifie(Principal principal, Authentication authentication);

}
