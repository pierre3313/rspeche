package fr.rsp.exemple.controller.impl;

import fr.rsp.exemple.controller.ExempleController;
import fr.rsp.exemple.model.UserExemple;
import fr.rsp.exemple.service.ServiceExemple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(value = "/")
public class ExempleControllerImpl implements ExempleController {

    ServiceExemple serviceExemple;

    @Autowired
    public ExempleControllerImpl(ServiceExemple serviceExemple){
        this.serviceExemple = serviceExemple;
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/users/simple")
    public UserExemple getSimpleUserExemple() {
        return serviceExemple.getSimpleUserExemple();
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/users/simplewithrole")
    @PreAuthorize("hasRole('ADMIN')")
    public UserExemple getSimpleUserExempleWithRoleAdmin() {
        return serviceExemple.getSimpleUserExemple();
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/users/simplewithscope")
    @PostAuthorize("hasAuthority('WRITE')")
    public UserExemple getSimpleUserExempleWithScope() {
        return serviceExemple.getSimpleUserExemple();
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/users/{id}")
    public UserExemple getUserExempleById(@PathVariable("id") Integer id) {
        return serviceExemple.getUserInBase(id);
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/users/authenticated")
    public Principal getUserAuthentifie(Principal principal, Authentication authentication) {

        UserExemple userExemple = new UserExemple();
        userExemple.setId(10);
        userExemple.setName(authentication.toString());

        return principal;
    }
}
