package fr.rsp.exemple.repository;

import fr.rsp.exemple.model.UserExemple;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExempleRepository extends CrudRepository<UserExemple, Integer>{

    Optional<UserExemple> findById(Integer id);

    List<UserExemple> findByName(String name);
}
