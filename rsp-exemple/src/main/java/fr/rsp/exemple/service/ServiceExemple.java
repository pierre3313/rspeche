package fr.rsp.exemple.service;

import fr.rsp.exemple.model.UserExemple;

public interface ServiceExemple {

    public UserExemple getSimpleUserExemple();

    public UserExemple getUserInBase(Integer id);
}
