package fr.rsp.exemple.service.impl;

import fr.rsp.exemple.model.UserExemple;
import fr.rsp.exemple.repository.ExempleRepository;
import fr.rsp.exemple.service.ServiceExemple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceExempleImpl implements ServiceExemple {

    ExempleRepository exempleRepository;

    @Autowired
    public ServiceExempleImpl(ExempleRepository exempleRepository){
        this.exempleRepository = exempleRepository;
    }

    @Override
    public UserExemple getSimpleUserExemple() {

        UserExemple userExemple = new UserExemple();
        userExemple.setName("User Test");

        return userExemple;
    }

    @Override
    public UserExemple getUserInBase(Integer id) {

        return exempleRepository.findById(id).get();
    }
}
