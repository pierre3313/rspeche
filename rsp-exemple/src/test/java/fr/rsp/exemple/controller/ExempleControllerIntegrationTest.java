package fr.rsp.exemple.controller;

import fr.rsp.exemple.ExempleApplication;
import fr.rsp.exemple.model.UserExemple;
import fr.rsp.exemple.repository.ExempleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = ExempleApplication.class)
@AutoConfigureMockMvc
/*@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")*/
public class ExempleControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExempleRepository exempleRepository;

    @Test
    @WithMockUser(username = "ram", roles={"ADMIN"})
    public void testGetSimpleUserExemple() throws Exception {

        this.mockMvc.perform(get("/users/simple")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("User Test"));
    }

    @Test
    @WithMockUser(username = "ram", roles={"ADMIN"})
    public void testGetUserExempleById() throws Exception {

        UserExemple userExemple = new UserExemple();
        userExemple.setId(1);
        userExemple.setName("User Test");

        given(exempleRepository.findById(anyInt())).willReturn(Optional.of(userExemple));

        this.mockMvc.perform(get("/users/1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("User Test"));
    }
}
