package fr.rsp.exemple.controller;

import fr.rsp.exemple.model.UserExemple;
import fr.rsp.exemple.repository.ExempleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ExempleControllerSansMockJPATest {

    @Autowired
    ExempleRepository exempleRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser(username = "ram", roles={"ADMIN"})
    public void testGetUserExempleById() throws Exception {

        UserExemple userExemple = new UserExemple();
        userExemple.setName("User Test");
        exempleRepository.save(userExemple);

        this.mockMvc.perform(get("/users/1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("User Test"));
    }
}
