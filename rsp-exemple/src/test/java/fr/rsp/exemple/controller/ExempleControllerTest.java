package fr.rsp.exemple.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.rsp.exemple.ExempleApplication;
import fr.rsp.exemple.model.UserExemple;
import fr.rsp.exemple.repository.ExempleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.anyInt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExempleApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ExempleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExempleRepository exempleRepository;

    @Test
    @WithMockUser(username = "ram", roles={"ADMIN"})
    public void testGetSimpleUserExemple() throws Exception {

        this.mockMvc.perform(get("/users/simple")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("User Test"));
    }

    @Test
    public void testForbiddenGetSimpleUserExempleWithRoleAdmin() throws Exception {
        this.mockMvc.perform(get("/users/simplewithrole")).andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "ram", roles={"ADMIN"})
    public void testOkGetSimpleUserExempleWithRoleAdmin() throws Exception {
        this.mockMvc.perform(get("/users/simplewithrole")).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "ram", roles={"BAD_ROLE"})
    public void testOkGetSimpleUserExempleWithBadRole() throws Exception {
        this.mockMvc.perform(get("/users/simplewithrole")).andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "client", authorities = {"BAD_SCOPE"})
    public void testOkGetSimpleUserExempleWithBadScope() throws Exception {
        this.mockMvc.perform(get("/users/simplewithscope")).andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "ram", roles={"ADMIN"})
    public void testGetUserAuthentifie() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/users/authenticated")).andDo(print())
                .andExpect(status().isOk()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        UserExemple userExemple = mapper.readValue(mvcResult.getResponse().getContentAsString(), UserExemple.class);

        System.out.println(userExemple.toString());
    }

    @Test
    @WithMockUser(username = "ram", roles={"ADMIN"})
    public void testGetUserExempleByIdWithRepositoryMock() throws Exception {

        UserExemple userExemple = new UserExemple();
        userExemple.setId(1);
        userExemple.setName("User Test");

        given(exempleRepository.findById(anyInt())).willReturn(Optional.of(userExemple));

        this.mockMvc.perform(get("/users/{id}","1")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("User Test"));

        then(exempleRepository)
                .should()
                .findById(1);
    }

}
