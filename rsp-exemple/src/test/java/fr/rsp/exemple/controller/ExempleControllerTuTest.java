package fr.rsp.exemple.controller;

import fr.rsp.exemple.controller.impl.ExempleControllerImpl;
import fr.rsp.exemple.model.UserExemple;
import fr.rsp.exemple.service.StubServiceExemple;
import org.apache.http.auth.BasicUserPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.security.Principal;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ExempleControllerTuTest {

    private ExempleController exempleController;

    @Before
    public void setUp(){
        exempleController = new ExempleControllerImpl(new StubServiceExemple());
    }

    @Test
    public void testGetSimpleUserExemple(){
        UserExemple userExemple = this.exempleController.getSimpleUserExemple();

        assertEquals(1, userExemple.getId());
        assertEquals("User Test", userExemple.getName());
    }

    @Test
    public void tesGetUserExempleById(){
        UserExemple userExemple = this.exempleController.getUserExempleById(1);

        assertEquals(1, userExemple.getId());
        assertEquals("User Test", userExemple.getName());
    }

    @Test
    public void testGetUserAuthentifie(){

        Principal principal = new BasicUserPrincipal("User Test");
        Authentication authentication = new TestingAuthenticationToken(principal, null);

        Principal userExemple = this.exempleController
                .getUserAuthentifie(principal, authentication);

        assertEquals(principal.getName(), userExemple.getName() );

    }

}
