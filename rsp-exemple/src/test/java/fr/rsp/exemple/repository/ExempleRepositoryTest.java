package fr.rsp.exemple.repository;


import fr.rsp.exemple.model.UserExemple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ExempleRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ExempleRepository exempleRepository;

    @Test
    public void testFindById(){

        UserExemple userExemple = new UserExemple();
        userExemple.setName("TEST");
        entityManager.persist(userExemple);

        Optional<UserExemple> u = exempleRepository.findById(1);
        assertTrue(u.isPresent());

        assertThat(u.get().getName(),is(equalTo("TEST")));
    }
}
