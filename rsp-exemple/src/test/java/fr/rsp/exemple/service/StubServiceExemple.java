package fr.rsp.exemple.service;

import fr.rsp.exemple.model.UserExemple;

public class StubServiceExemple implements ServiceExemple {

    private UserExemple userExemple;

    public StubServiceExemple(){
        this.userExemple = new UserExemple();
        userExemple.setId(1);
        userExemple.setName("User Test");
    }

    @Override
    public UserExemple getSimpleUserExemple() {
        return userExemple;
    }

    @Override
    public UserExemple getUserInBase(Integer id) {
        return userExemple;
    }
}
