package fr.rsp.gateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayZuulApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GatewayZuulApplicationTest {

    @Test
    public void contextLoads() {

    }

    @Test
    public void contextLoadsPlus(){
        GatewayZuulApplication.main(new String[]{
                "--spring.main.web-environment=false"
        });
    }
}
