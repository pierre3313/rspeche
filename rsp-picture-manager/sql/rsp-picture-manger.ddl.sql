CREATE TABLE picture{
    picture_id VARCHAR(128),
    picture_name VARCHAR(128),
    picture_type VARCHAR(128),
    picture_res_x INT,
    picture_res_y INT,
    user_id BIGINT,
    PRIMARY KEY(picture_id)
}ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE picture_content{
    picture_id VARCHAR(128)
    picture_min_content BLOB,
    picture_content BLOB,
    PRIMARY KEY(picture_id)
}ENGINE=InnoDB DEFAULT CHARSET=utf8;