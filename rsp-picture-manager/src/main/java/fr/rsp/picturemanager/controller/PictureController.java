package fr.rsp.picturemanager.controller;

import fr.rsp.picturemanager.model.Picture;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public interface PictureController {

    public ResponseEntity<Resource> getPicture(String id);

    public ResponseEntity<Resource> getPictureMiniature(String id);

    public ResponseEntity<Picture> getPictureMetaData(String id);

    public ResponseEntity<Picture> uploadPicture(MultipartFile file, RedirectAttributes redirectAttributes);

    public ResponseEntity deletePicture(String id);
}
