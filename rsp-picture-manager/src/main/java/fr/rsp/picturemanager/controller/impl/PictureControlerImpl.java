package fr.rsp.picturemanager.controller.impl;

import fr.rsp.picturemanager.controller.PictureController;
import fr.rsp.picturemanager.model.Picture;
import fr.rsp.picturemanager.service.ServicePicture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@RequestMapping("/picture")
public class PictureControlerImpl implements PictureController{

    private ServicePicture servicePicture;

    @Autowired
    public PictureControlerImpl(ServicePicture servicePicture){
        this.servicePicture = servicePicture;
    }

    @Override
    @GetMapping(value = "/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<Resource> getPicture(@PathVariable String id) {

        Picture picture = servicePicture.searchPictureMetaData(id);
        Resource pictureContent = servicePicture.searchPictureContent(id);

        if(pictureContent != null){
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=\"" + picture.getPicture_name() + "\"").body(pictureContent);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @GetMapping(value = "/min/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<Resource> getPictureMiniature(@PathVariable String id) {

        Picture picture = servicePicture.searchPictureMetaData(id);
        Resource pictureMinContent = servicePicture.searchPictureMinContent(id);

        if(pictureMinContent != null){
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=\"" + picture.getPicture_name() + "\"").body(pictureMinContent);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @GetMapping(value = "/{id}/meta")
    public ResponseEntity<Picture> getPictureMetaData(@PathVariable String id) {

        Picture picture = servicePicture.searchPictureMetaData(id);
        if(picture != null){
            return new ResponseEntity<>(picture, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @PostMapping(value = "/")
    public ResponseEntity<Picture> uploadPicture(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

        Picture picture = servicePicture.storePicture(file);
        return new ResponseEntity<>(picture, HttpStatus.OK);
    }

    @Override
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deletePicture(@PathVariable String id) {

        try{
            servicePicture.deletePicture(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
