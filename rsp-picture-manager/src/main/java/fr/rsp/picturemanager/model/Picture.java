package fr.rsp.picturemanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "picture")
public class Picture implements Serializable{

    @Id
    @Column(name = "picture_id", nullable = false, updatable = false)
    private String picture_id;

    @Column(name="picture_name")
    private String picture_name;

    @Column(name="picture_type")
    private String picture_type;

    @Column(name="picture_res_x")
    private int picture_res_x;

    @Column(name="picture_res_y")
    private int picture_res_y;

    @Column(name="user_id")
    private int user_id;

    public String getPicture_id() {
        return picture_id;
    }

    public void setPicture_id(String picture_id) {
        this.picture_id = picture_id;
    }

    public String getPicture_name() {
        return picture_name;
    }

    public void setPicture_name(String picture_name) {
        this.picture_name = picture_name;
    }

    public String getPicture_type() {
        return picture_type;
    }

    public void setPicture_type(String picture_type) {
        this.picture_type = picture_type;
    }

    public int getPicture_res_x() {
        return picture_res_x;
    }

    public void setPicture_res_x(int picture_res_x) {
        this.picture_res_x = picture_res_x;
    }

    public int getPicture_res_y() {
        return picture_res_y;
    }

    public void setPicture_res_y(int picture_res_y) {
        this.picture_res_y = picture_res_y;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "picture_id='" + picture_id + '\'' +
                ", picture_name='" + picture_name + '\'' +
                ", picture_type='" + picture_type + '\'' +
                ", picture_res_x=" + picture_res_x +
                ", picture_res_y=" + picture_res_y +
                ", user_id=" + user_id +
                '}';
    }
}
