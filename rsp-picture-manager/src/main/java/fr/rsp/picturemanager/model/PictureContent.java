package fr.rsp.picturemanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "picture_content")
public class PictureContent implements Serializable{

    @Id
    @Column(name = "picture_id", nullable = false, updatable = false)
    private String picture_id;

    @Column(name="picture_min_content", length=100000000)
    private byte[] picture_min_content;

    @Column(name="picture_content", length=100000000)
    private byte[] picture_content;

    public byte[] getPicture_min_content() {
        return picture_min_content;
    }

    public void setPicture_min_content(byte[] picture_min_content) {
        this.picture_min_content = picture_min_content;
    }

    public byte[] getPicture_content() {
        return picture_content;
    }

    public void setPicture_content(byte[] picture_content) {
        this.picture_content = picture_content;
    }

    public String getPicture_id() {
        return picture_id;
    }

    public void setPicture_id(String picture_id) {
        this.picture_id = picture_id;
    }
}
