package fr.rsp.picturemanager.repository;

import fr.rsp.picturemanager.model.PictureContent;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PictureContentRepository extends CrudRepository<PictureContent, String> {

    @Override
    Optional<PictureContent> findById(String s);

    @Override
    <S extends PictureContent> S save(S s);

    @Override
    void delete(PictureContent pictureContent);
}
