package fr.rsp.picturemanager.repository;

import fr.rsp.picturemanager.model.Picture;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PictureRepository extends CrudRepository<Picture, String>{

    @Override
    Optional<Picture> findById(String s);

    @Override
    <S extends Picture> S save(S s);

    @Override
    void delete(Picture picture);
}
