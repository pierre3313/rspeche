package fr.rsp.picturemanager.service;

import fr.rsp.picturemanager.model.Picture;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

public interface ServicePicture {

    /**
     * Récupére le contenu des images en fonction de l'id
     * @param picture_id
     * @return
     */
    public Resource searchPictureContent(String picture_id);

    /**
     * Récupére le contenu en mode miniature des images en fonction de l'id
     * @param picture_id
     * @return
     */
    public Resource searchPictureMinContent(String picture_id);

    /**
     * Récupere les meta data des images
     * @param picture_id
     * @return
     */
    public Picture searchPictureMetaData(String picture_id);

    /**
     * Stockage de l'image en BDD, création de la miniature, récupération des META DATA
     * @param file
     * @return
     */
    public Picture storePicture(MultipartFile file);

    /**
     * Suppression de l'image et de ces meta data
     * @param picture_id
     */
    @Transactional
    public void deletePicture(String picture_id) throws Exception;
}
