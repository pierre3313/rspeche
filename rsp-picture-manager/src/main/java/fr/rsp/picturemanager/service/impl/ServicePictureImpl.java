package fr.rsp.picturemanager.service.impl;

import fr.rsp.picturemanager.model.Picture;
import fr.rsp.picturemanager.model.PictureContent;
import fr.rsp.picturemanager.repository.PictureContentRepository;
import fr.rsp.picturemanager.repository.PictureRepository;
import fr.rsp.picturemanager.service.ServicePicture;
import fr.rsp.picturemanager.exception.StorageException;
import fr.rsp.picturemanager.util.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.logging.Logger;


@Service
public class ServicePictureImpl implements ServicePicture{

    private static Logger LOGGER = Logger.getLogger(ServicePicture.class.getName());

    private static final int MAX_IMAGE_WIDTH = 1920;
    private static final int MAX_IMAGE_HEIGHT = 1080;

    private static final int MAX_IMAGE_MIN_WIDTH = 500;
    private static final int MAX_IMAGE_MIN_HEIGHT = 334;

    private PictureContentRepository pictureContentRepository;

    private PictureRepository pictureRepository;

    @Autowired
    public ServicePictureImpl(PictureContentRepository pictureContentRepository,
                              PictureRepository pictureRepository){
        this.pictureContentRepository = pictureContentRepository;
        this.pictureRepository = pictureRepository;
    }

    @Override
    public Resource searchPictureContent(String picture_id){

        Optional<PictureContent> pictureContent = pictureContentRepository.findById(picture_id);
        if(pictureContent.isPresent())
            return new ByteArrayResource(pictureContent.get().getPicture_content());

        return null;
    }

    @Override
    public Resource searchPictureMinContent(String picture_id) {


        Optional<PictureContent> pictureContent = pictureContentRepository.findById(picture_id);
        if(pictureContent.isPresent())
            return new ByteArrayResource(pictureContent.get().getPicture_min_content());

        return null;
    }

    @Override
    public Picture searchPictureMetaData(String picture_id){

        Optional<Picture> picture = pictureRepository.findById(picture_id);
        if(picture.isPresent())
            return picture.get();

        return null;
    }

    @Override
    public Picture storePicture(MultipartFile file) {

        String picture_name = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Impossible de stocker un fichier vide " + picture_name);
            }
            if (picture_name.contains("..")) {
                throw new StorageException(
                        "Impossible de stocker un fichier à partir d'un chemin relatif " + picture_name);
            }
            String mimeType = file.getContentType();
            String extension = picture_name.substring(picture_name.lastIndexOf('.')+1);
            if(!"jpg".equals(extension) && !"png".equals(extension)
                        && !MimeTypeUtils.IMAGE_JPEG_VALUE.equals(mimeType)&& !MimeTypeUtils.IMAGE_PNG_VALUE.equals(mimeType)){
                throw new StorageException(
                        "Le format de l'image n'est pas reconnu. Format accepté : jpg ou png " + picture_name);
            }

            //Lecture du contenu de l'image
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int read;
            byte[] bytes = new byte[1024];

            InputStream is = file.getInputStream();
            while ((read = is.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            byte[] pictureContentByte = out.toByteArray();

            //Conversion si nécessaire du jpg en png
            if("png".equals(extension) || MimeTypeUtils.IMAGE_PNG_VALUE.equals(mimeType)){
                pictureContentByte = ImageUtils.convertPngToJpg(pictureContentByte);
            }

            //Traitement de l'image
            int width = ImageUtils.getImageWidth(pictureContentByte);
            int height = ImageUtils.getImageHeight(pictureContentByte);

            if(width > MAX_IMAGE_WIDTH){
                float ratio = (float)MAX_IMAGE_WIDTH/width;
                pictureContentByte = ImageUtils.resizeImage(pictureContentByte, MAX_IMAGE_WIDTH, Math.round(height*ratio) );
                width = MAX_IMAGE_WIDTH;
                height = Math.round(height*ratio);
            }else if(height > MAX_IMAGE_HEIGHT){
                float ratio = (float)MAX_IMAGE_HEIGHT/height;
                pictureContentByte = ImageUtils.resizeImage(pictureContentByte, Math.round(width*ratio), MAX_IMAGE_HEIGHT );
                width = Math.round(width*ratio);
                height = MAX_IMAGE_HEIGHT;
            }

            //Traitement de la miniature
            byte[] pictureMinContentByte = pictureContentByte;

            if(width > MAX_IMAGE_MIN_WIDTH){
                float ratioMin = (float)MAX_IMAGE_MIN_WIDTH/width;
                pictureMinContentByte = ImageUtils.resizeImage(pictureContentByte, MAX_IMAGE_MIN_WIDTH, Math.round(height*ratioMin) );
            }else if(height > MAX_IMAGE_MIN_HEIGHT){
                float ratioMin = (float)MAX_IMAGE_MIN_HEIGHT/height;
                pictureMinContentByte = ImageUtils.resizeImage(pictureContentByte, Math.round(width*ratioMin), MAX_IMAGE_MIN_HEIGHT );
            }

            //Persistence des méta données
            String picture_id = ImageUtils.hashByteImageWithMD5(pictureContentByte);

            Picture picture = new Picture();
            picture.setPicture_id(picture_id);
            picture.setPicture_name(picture_name);
            picture.setPicture_res_x(width);
            picture.setPicture_res_y(height);
            picture.setPicture_type(MimeTypeUtils.IMAGE_PNG_VALUE);

            picture = pictureRepository.save(picture);

            //Persistence du contenu de l'image
            PictureContent pictureContent = new PictureContent();
            pictureContent.setPicture_id(picture_id);
            pictureContent.setPicture_content(pictureContentByte);
            pictureContent.setPicture_min_content(pictureMinContentByte);

            pictureContentRepository.save(pictureContent);

            return picture;
        }
        catch (IOException e) {
            throw new StorageException("Impossible d'enregistrer le fichier en BDD voir " +
                    "l'exception pour plus de détail " + picture_name, e);
        } catch (NoSuchAlgorithmException e) {
            throw new StorageException("Impossible d'enregistrer le fichier en BDD voir " +
                    "l'exception pour plus de détail " + picture_name, e);
        }
    }

    @Override
    public void deletePicture(String picture_id) throws Exception{

        //Suppression du contenu
        Optional<PictureContent> pictureContent = pictureContentRepository.findById(picture_id);
        if(pictureContent.isPresent())
            pictureContentRepository.delete(pictureContent.get());
        else
            throw new Exception("Id inconnu : " + picture_id);

        //Suppression des meta data
        Optional<Picture> picture = pictureRepository.findById(picture_id);
        if(picture.isPresent())
            pictureRepository.delete(picture.get());
        else
            throw new Exception("Id inconnu : " + picture_id);
    }
}
