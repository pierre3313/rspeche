package fr.rsp.picturemanager.util;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class comprenant des méthodes statiques pour la manipulation des images
 */
public class ImageUtils {

    /**
     * Permet de retaille une image
     * @param imageToResize
     * @param width
     * @param height
     * @return
     * @throws IOException
     */
    public static byte[] resizeImage(byte[] imageToResize, int width, int height) throws IOException {

        InputStream is = new ByteArrayInputStream(imageToResize);
        BufferedImage bufferedImage = ImageIO.read(is);

        BufferedImage bufferedImageOut = ImageUtils.resizeImage(bufferedImage, width, height);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImageOut, "jpg", baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();

        return imageInByte;
    }

    /**
     * Permet de retaille une image
     * @param imageToResize
     * @param width
     * @param height
     * @return
     * @throws IOException
     */
    public static BufferedImage resizeImage(BufferedImage imageToResize, int width, int height) throws IOException {

        //Image tmp = imageToResize.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        BufferedImage resized = new BufferedImage(width, height, imageToResize.getType());
        Graphics2D g2d = resized.createGraphics();

        g2d.setComposite(AlphaComposite.Src);

        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        g2d.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2d.drawImage(imageToResize, 0, 0, width, height, null);
        g2d.dispose();

        return resized;
    }

    /**
     * Donne le width de l'image
     * @param byteImage
     * @return
     * @throws IOException
     */
    public static int getImageWidth(byte[] byteImage) throws IOException {

        InputStream is = new ByteArrayInputStream(byteImage);
        BufferedImage bufferedImage = ImageIO.read(is);

        return ImageUtils.getImageWidth(bufferedImage);
    }

    /**
     * Donne le width de l'image
     * @param bufferedImage
     * @return
     * @throws IOException
     */
    public static int getImageWidth(BufferedImage bufferedImage) throws IOException {
        return bufferedImage.getWidth();
    }

    /**
     * Donne le height de l'image
     * @param byteImage
     * @return
     * @throws IOException
     */
    public static int getImageHeight(byte[] byteImage) throws IOException {
        InputStream is = new ByteArrayInputStream(byteImage);
        BufferedImage bufferedImage = ImageIO.read(is);

        return ImageUtils.getImageHeight(bufferedImage);
    }

    /**
     * Donne le height de l'image
     * @param bufferedImage
     * @return
     * @throws IOException
     */
    public static int getImageHeight(BufferedImage bufferedImage) throws IOException {
        return bufferedImage.getHeight();
    }

    /**
     * Donne la valeur du hash MD5 d'un tableau de byte
     * @param byteImage
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String hashByteImageWithMD5(byte[] byteImage) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(byteImage);
        byte[] digest = md.digest();

        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }

    /**
     * Convertit un png en jpg
     * @param imagePng
     * @return
     * @throws IOException
     */
    public static byte[] convertPngToJpg(byte[] imagePng) throws IOException {

        InputStream is = new ByteArrayInputStream(imagePng);
        BufferedImage bufferedImage = ImageIO.read(is);

        return convertPngToJpg(bufferedImage);
    }

    /**
     * Convertit un png en jpg
     * @param bufferedImagePng
     * @return
     * @throws IOException
     */
    public static byte[] convertPngToJpg(BufferedImage bufferedImagePng) throws IOException {

        BufferedImage bufferedImageJpg = new BufferedImage(bufferedImagePng.getWidth(),
                bufferedImagePng.getHeight(), BufferedImage.TYPE_INT_RGB);
        bufferedImageJpg.createGraphics().drawImage(bufferedImagePng, 0, 0, Color.WHITE, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ImageIO.write(bufferedImageJpg, "jpg", baos);

        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();

        return imageInByte;
    }
}
