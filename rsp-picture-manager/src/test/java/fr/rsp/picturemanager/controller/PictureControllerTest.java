package fr.rsp.picturemanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.rsp.picturemanager.PictureManagerApplication;
import fr.rsp.picturemanager.model.Picture;
import fr.rsp.picturemanager.model.PictureContent;
import fr.rsp.picturemanager.repository.PictureContentRepository;
import fr.rsp.picturemanager.repository.PictureRepository;
import fr.rsp.picturemanager.service.ServicePicture;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.MimeTypeUtils;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PictureManagerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PictureControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PictureContentRepository pictureContentRepository;

    @Autowired
    private PictureRepository pictureRepository;

    @Autowired
    private ServicePicture ServicePicture;

    private static Picture picture;

    @Before
    public void setUp() throws Exception {
        InputStream fis = this.getClass().getClassLoader().getResourceAsStream("phototest.jpg");

        MockMultipartFile multipartFile = new MockMultipartFile("file", "phototest.jpg",
                MimeTypeUtils.IMAGE_JPEG_VALUE, fis);

       picture = ServicePicture.storePicture(multipartFile);
    }

    @Test
    public void testGetPicture() throws Exception {

        this.mockMvc.perform(get("/picture/"+picture.getPicture_id())).andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void testGetPicture_notFound() throws Exception {

        this.mockMvc.perform(get("/picture/123456")).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetPictureMiniature() throws  Exception {

        this.mockMvc.perform(get("/picture/min/"+picture.getPicture_id())).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPictureMiniature_notFound() throws  Exception {

        this.mockMvc.perform(get("/picture/min/123456")).andDo(print())
                .andExpect(status().isNotFound());
    }


    @Test
    public void testGetPictureMetaData() throws  Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/picture/"+picture.getPicture_id()+"/meta")).andDo(print())
                .andExpect(status().isOk()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        Picture pic = mapper.readValue(mvcResult.getResponse().getContentAsString(),Picture.class);

        Assert.assertEquals(pic.getPicture_id(), picture.getPicture_id());
        Assert.assertEquals(pic.getPicture_name(), picture.getPicture_name());
        Assert.assertEquals(pic.getPicture_res_x(), picture.getPicture_res_x());
        Assert.assertEquals(pic.getPicture_res_y(), picture.getPicture_res_y());
        Assert.assertEquals(pic.getUser_id(), picture.getUser_id());
    }

    @Test
    public void testGetPictureMetaData_notFound() throws  Exception {

        this.mockMvc.perform(get("/picture/123456/meta")).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeletePicture() throws  Exception {

        this.mockMvc.perform(delete("/picture/"+picture.getPicture_id())).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testDeletePicture_notFound() throws  Exception {

        this.mockMvc.perform(delete("/picture/123456")).andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUploadPicture() throws Exception{

        InputStream fis = this.getClass().getClassLoader().getResourceAsStream("phototest.jpg");

        MockMultipartFile multipartFile = new MockMultipartFile("file", "phototest.jpg",
                MimeTypeUtils.IMAGE_JPEG_VALUE, fis);

        MvcResult mvcResult = this.mockMvc.perform(fileUpload("/picture/").file(multipartFile))
                .andExpect(status().isOk()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        Picture picture = mapper.readValue(mvcResult.getResponse().getContentAsString(),Picture.class);

        Optional<PictureContent> pictureContent = pictureContentRepository.findById(picture.getPicture_id());
        FileOutputStream fos = new FileOutputStream("target/test-classes/phototest-bdd.jpg");
        fos.write(pictureContent.get().getPicture_content());
        fos.close();

        FileOutputStream fosmin = new FileOutputStream("target/test-classes/phototest-min-bdd.jpg");
        fosmin.write(pictureContent.get().getPicture_min_content());
        fosmin.close();
    }
}
