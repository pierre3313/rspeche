package fr.rsp.picturemanager.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@RunWith(SpringRunner.class)
public class ImageUtilsTest {

    @Test
    public void testResizeImage() throws IOException{

        Path path = Paths.get("./src/test/resources/phototest.jpg");
        byte[] byteFile = Files.readAllBytes(path);

        byteFile = ImageUtils.resizeImage(byteFile, 1920, 1440);

        FileOutputStream fileOuputStream = new FileOutputStream("target/test-classes/phototest-resize.jpg");
        fileOuputStream.write(byteFile);
    }
}
