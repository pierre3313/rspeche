package fr.rsp.profile.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    public static final boolean isResourceStatless = true;

    @Bean
    public boolean setResourceStateless(){
        return ApplicationConfiguration.isResourceStatless;
    }
}
