package fr.rsp.profile.controller;

import fr.rsp.profile.controller.dto.ProfileDto;
import org.springframework.http.ResponseEntity;

import java.security.Principal;

/**
 * Interface du controller profile
 */
public interface ProfileController {

    /**
     * Enregistrer un nouveau profile
     * @param profileDto
     * @return
     */
    public ResponseEntity<ProfileDto> registerProfile(ProfileDto profileDto);

    /**
     * Renvoie le profile de l'utilisateur authentifié
     * @param principal
     * @return
     */
    public ResponseEntity<ProfileDto> getProfile(Principal principal);

    /**
     * Renvoie le profile d'un utilisateur via son profileId
     * Réservé aux users ADMIN
     * @param id
     * @return
     */
    public ResponseEntity<ProfileDto> getProfile(String id);

    /**
     * Met à jour le profile d'un utilisateur
     * @param id
     * @param profileDto
     * @return
     */
    public ResponseEntity<ProfileDto> updateProfile(String id, ProfileDto profileDto);

}
