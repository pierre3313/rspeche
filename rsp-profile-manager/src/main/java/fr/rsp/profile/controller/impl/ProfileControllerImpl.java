package fr.rsp.profile.controller.impl;

import fr.rsp.profile.controller.ProfileController;
import fr.rsp.profile.controller.dto.ProfileDto;
import fr.rsp.profile.controller.mapping.MapProfileDto;
import fr.rsp.profile.exception.ProfileException;
import fr.rsp.profile.model.Profile;
import fr.rsp.profile.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/profile")
public class ProfileControllerImpl implements ProfileController {

    private ProfileService profileService;

    @Autowired
    public ProfileControllerImpl(ProfileService profileService){
        this.profileService = profileService;
    }

    @Override
    @PostMapping
    public ResponseEntity<ProfileDto> registerProfile(@RequestBody ProfileDto profileDto) {

        Profile profile = profileService.saveProfile(MapProfileDto.mapProfileDtoToProfile(profileDto));
        return new ResponseEntity<>(MapProfileDto.mapProfileToProfileDto(profile), HttpStatus.OK);
    }

    @Override
    @GetMapping
    @PreAuthorize("hasAuthority('RSP_USER_READ') or hasAuthority('RSP_ADMIN_READ')")
    public ResponseEntity<ProfileDto> getProfile(Principal principal) {

        try{
            Profile profile = profileService.findProfileByEmail(principal.getName());
            return new ResponseEntity<>(MapProfileDto.mapProfileToProfileDto(profile), HttpStatus.OK);
        }catch (ProfileException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @Override
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('RSP_ADMIN_READ')")
    public ResponseEntity<ProfileDto> getProfile(@PathVariable String id) {

        try{
            Profile profile = profileService.findProfileById(id);
            return new ResponseEntity<>(MapProfileDto.mapProfileToProfileDto(profile), HttpStatus.OK);
        }catch (ProfileException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @Override
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('RSP_USER_READ') or hasAuthority('RSP_ADMIN_READ')")
    public ResponseEntity<ProfileDto> updateProfile(@PathVariable String id, @RequestBody ProfileDto profileDto) {
        return null;
    }
}
