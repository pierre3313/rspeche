package fr.rsp.profile.controller.mapping;

import fr.rsp.profile.controller.dto.ProfileDto;
import fr.rsp.profile.model.Profile;

import java.util.Date;

public class MapProfileDto {

    public static Profile mapProfileDtoToProfile(ProfileDto profileDto){

        Profile profile = new Profile();

        profile.setFirstName(profileDto.getFirstName());
        profile.setLastName(profileDto.getLastName());
        profile.setEmail(profileDto.getEmail());
        profile.setBlog(profileDto.getBlog());
        profile.setProfileId(profileDto.getProfileId());
        profile.setTown(profileDto.getTown());

        profile.setBirthday(new java.sql.Date(profileDto.getBirthday().getTime()));

        return profile;
    }

    public static ProfileDto mapProfileToProfileDto(Profile profile){

        ProfileDto profileDto = new ProfileDto();

        profileDto.setFirstName(profile.getFirstName());
        profileDto.setLastName(profile.getLastName());
        profileDto.setEmail(profile.getEmail());
        profileDto.setBlog(profile.getBlog());
        profileDto.setProfileId(profile.getProfileId());
        profileDto.setTown(profile.getTown());

        profileDto.setBirthday(new Date(profile.getBirthday().getTime()));
        profileDto.setRegistration(new Date(profile.getRegistration().getTime()));

        return profileDto;
    }
}
