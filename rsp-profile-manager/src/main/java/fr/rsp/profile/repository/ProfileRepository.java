package fr.rsp.profile.repository;

import fr.rsp.profile.model.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfileRepository extends CrudRepository<Profile, String> {

    @Override
    Optional<Profile> findById(String s);

    Optional<Profile> findByEmail(String s);

}
