package fr.rsp.profile.service;

import fr.rsp.profile.model.Profile;

import java.security.NoSuchAlgorithmException;

/**
 * Service de gestion des profiles
 */
public interface ProfileService {

    public Profile findProfileById(String profileId);

    public Profile findProfileByEmail(String email);

    public Profile saveProfile(Profile profile);

    public String generateProfileId_withMd5(String email) throws NoSuchAlgorithmException;
}
