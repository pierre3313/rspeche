package fr.rsp.profile.service.impl;

import fr.rsp.profile.exception.ProfileException;
import fr.rsp.profile.model.Profile;
import fr.rsp.profile.repository.ProfileRepository;
import fr.rsp.profile.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ProfileServiceImpl implements ProfileService {

    private static final Logger logger = Logger.getLogger(ProfileServiceImpl.class.getName());

    private ProfileRepository profileRepository;

    @Autowired
    public ProfileServiceImpl(ProfileRepository profileRepository){
        this.profileRepository = profileRepository;
    }

    @Override
    public Profile findProfileById(String profileId) {

        Optional<Profile> profile = profileRepository.findById(profileId);
        if(!profile.isPresent())
            throw new ProfileException("Profile " + profileId + " non trouvé");

        return profile.get();
    }

    @Override
    public Profile findProfileByEmail(String email) {

        Optional<Profile> profile = profileRepository.findByEmail(email);
        if(!profile.isPresent())
            throw new ProfileException("Profile par email " + email + " non trouvé");

        return profile.get();
    }

    @Override
    public Profile saveProfile(Profile profile){

        try {
            String profileId = generateProfileId_withMd5(profile.getEmail());
            profile.setProfileId(profileId);
            profile.setRegistration(Date.valueOf(LocalDate.now()));
        } catch (NoSuchAlgorithmException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new ProfileException("Impossible d'enregistrer un profile");
        }

        return profileRepository.save(profile);
    }

    public String generateProfileId_withMd5(String email) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(email.getBytes());
        byte[] digest = md.digest();
        String hash = DatatypeConverter
                .printHexBinary(digest).toUpperCase();

        return hash;
    }
}
