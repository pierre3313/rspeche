package fr.rsp.profile;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProfileApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProfileApplicationTest {

    @Test
    public void contextLoads() {

    }

    @Test
    public void contextLoadsPlus(){
        ProfileApplication.main(new String[]{
                "--spring.main.web-environment=false"
        });
    }
}
