package fr.rsp.profile.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ApplicationConfigurationForTest {

    public static final boolean isResourceStatless = false;

    /**
     *  Bean pour modifier le comportement du filter OAuth2AuthenticationProcessingFilter
     *  Le fait de positionner à false permet de basculer sur le securityContext et donc
     *  d'utiliser les mockUser en test
    */
    @Bean
    @Profile("test")
    public boolean setResourceStateless(){
        System.out.println("** IS NOT RESOURCE SECURITY STATELESS");
        return ApplicationConfigurationForTest.isResourceStatless;
    }
}
