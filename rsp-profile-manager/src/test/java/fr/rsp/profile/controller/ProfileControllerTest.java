package fr.rsp.profile.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.rsp.profile.ProfileApplication;
import fr.rsp.profile.config.ApplicationConfigurationForTest;
import fr.rsp.profile.controller.dto.ProfileDto;
import fr.rsp.profile.controller.mapping.MapProfileDto;
import fr.rsp.profile.model.Profile;
import fr.rsp.profile.model.ProfileForTest;
import fr.rsp.profile.repository.ProfileRepository;
import fr.rsp.profile.service.ProfileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static junit.framework.TestCase.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = ProfileApplication.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = ApplicationConfigurationForTest.class)
@ActiveProfiles("test")
public class ProfileControllerTest {

    @Autowired
    private WebApplicationContext webapp;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private ProfileService profileService;

    @Test
    public void testRegisterProfile() throws Exception {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        ObjectMapper mapper = new ObjectMapper();

        MvcResult mvcResult = mockMvc.perform(post("/profile")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(MapProfileDto.mapProfileToProfileDto(profileForTest))))
                .andExpect(status().isOk()).andReturn();

        //Test du retour du service REST
        ProfileDto profileReturn = mapper.readValue(mvcResult.getResponse().getContentAsString(), ProfileDto.class);

        assertEquals(profileService.generateProfileId_withMd5(profileForTest.getEmail()), profileReturn.getProfileId());
        assertEquals(profileForTest.getEmail(), profileReturn.getEmail());
        assertEquals(profileForTest.getFirstName(), profileReturn.getFirstName());
        assertEquals(profileForTest.getLastName(), profileReturn.getLastName());
        assertEquals(profileForTest.getBlog(), profileReturn.getBlog());
        assertEquals(profileForTest.getTown(), profileReturn.getTown());
        assertThat(profileForTest.getBirthday()).isEqualTo(profileReturn.getBirthday());
        assertThat(profileForTest.getRegistration()).isEqualTo(profileReturn.getRegistration());

    }

    @Test
    @WithMockUser(username = "test@email.fr", authorities={"RSP_ADMIN_READ"})
    public void testGetProfileById_withProfileRSP_ADMIN_READ() throws Exception {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        profileService.saveProfile(profileForTest);

        ObjectMapper mapper = new ObjectMapper();

        MvcResult mvcResult = mockMvc.perform(
                get("/profile/" + profileService.generateProfileId_withMd5(profileForTest.getEmail())))
                .andExpect(status().isOk()).andReturn();

        //Test du retour du service REST
        ProfileDto profileReturn = mapper.readValue(mvcResult.getResponse().getContentAsString(), ProfileDto.class);

        assertEquals(profileService.generateProfileId_withMd5(profileForTest.getEmail()), profileReturn.getProfileId());
        assertEquals(profileForTest.getEmail(), profileReturn.getEmail());
        assertEquals(profileForTest.getFirstName(), profileReturn.getFirstName());
        assertEquals(profileForTest.getLastName(), profileReturn.getLastName());
        assertEquals(profileForTest.getBlog(), profileReturn.getBlog());
        assertEquals(profileForTest.getTown(), profileReturn.getTown());
        assertThat(profileForTest.getBirthday()).isEqualTo(profileReturn.getBirthday());
        assertThat(profileForTest.getRegistration()).isEqualTo(profileReturn.getRegistration());
    }

    @Test
    @WithMockUser(username = "test@email.fr", authorities={"RSP_USER_READ"})
    public void testGetProfileById_withProfileRSP_USER_READ() throws Exception {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        profileService.saveProfile(profileForTest);

        mockMvc.perform(
                get("/profile/" + profileService.generateProfileId_withMd5(profileForTest.getEmail())))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetProfileById_withinProfile() throws Exception {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        profileService.saveProfile(profileForTest);

        mockMvc.perform(
                get("/profile/" + profileService.generateProfileId_withMd5(profileForTest.getEmail())))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "test@email.fr", authorities={"RSP_ADMIN_READ"})
    public void testGetProfileById_with404() throws Exception {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        profileService.saveProfile(profileForTest);

        mockMvc.perform(
                get("/profile/notfound"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "test@email.fr", authorities={"RSP_USER_READ"})
    public void testGetProfile_withRSP_USER_READ() throws Exception {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        profileService.saveProfile(profileForTest);

        ObjectMapper mapper = new ObjectMapper();

        MvcResult mvcResult = mockMvc.perform(
                get("/profile"))
                .andExpect(status().isOk()).andReturn();

        //Test du retour du service REST
        ProfileDto profileReturn = mapper.readValue(mvcResult.getResponse().getContentAsString(), ProfileDto.class);

        assertEquals(profileService.generateProfileId_withMd5(profileForTest.getEmail()), profileReturn.getProfileId());
        assertEquals(profileForTest.getEmail(), profileReturn.getEmail());
        assertEquals(profileForTest.getFirstName(), profileReturn.getFirstName());
        assertEquals(profileForTest.getLastName(), profileReturn.getLastName());
        assertEquals(profileForTest.getBlog(), profileReturn.getBlog());
        assertEquals(profileForTest.getTown(), profileReturn.getTown());
        assertThat(profileForTest.getBirthday()).isEqualTo(profileReturn.getBirthday());
        assertThat(profileForTest.getRegistration()).isEqualTo(profileReturn.getRegistration());
    }

    @Test
    @WithMockUser(username = "test@email.fr", authorities={"RSP_ADMIN_READ"})
    public void testGetProfile_withRSP_ADMIN_READ() throws Exception {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        profileService.saveProfile(profileForTest);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(
                get("/profile"))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void testGetProfile_withinProfile() throws Exception {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        profileService.saveProfile(profileForTest);

        mockMvc.perform(
                get("/profile"))
                .andExpect(status().isUnauthorized());
    }
}
