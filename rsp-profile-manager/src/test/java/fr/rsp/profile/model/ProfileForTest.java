package fr.rsp.profile.model;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.LocalDate;

public class ProfileForTest {

    public static Profile getProfileForTest(){

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update("test@email.fr".getBytes());
        byte[] digest = md.digest();
        String hash = DatatypeConverter
                .printHexBinary(digest).toUpperCase();

        Profile profile = new Profile();
        profile.setProfileId(hash);
        profile.setEmail("test@email.fr");
        profile.setFirstName("Prénom TEST");
        profile.setLastName("Nom TEST");
        profile.setBlog("http://psoi.fr");
        profile.setTown("Villenave d'ornon");
        Date birthday = Date.valueOf(LocalDate.of(1984, 4, 22));
        profile.setBirthday(birthday);

        Date registration = Date.valueOf(LocalDate.now());
        profile.setRegistration(registration);

        return profile;
    }

}
