package fr.rsp.profile.repository;

import fr.rsp.profile.model.Profile;
import fr.rsp.profile.model.ProfileForTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProfileRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProfileRepository profileRepository;

    Profile profile;

    @Before
    public void setUp(){
        this.profile = ProfileForTest.getProfileForTest();
    }

    @Test
    public void testfindById(){

        entityManager.persist(profile);

        Optional<Profile> p = profileRepository.findById(profile.getProfileId());

        assertTrue(p.isPresent());
        assertEquals(profile.getProfileId(), p.get().getProfileId());
        assertEquals(profile.getEmail(), p.get().getEmail());
        assertEquals(profile.getFirstName(), p.get().getFirstName());
        assertEquals(profile.getLastName(), p.get().getLastName());
        assertEquals(profile.getTown(), p.get().getTown());
        assertThat(profile.getBirthday()).isEqualTo(p.get().getBirthday());
        assertThat(profile.getRegistration()).isEqualTo(p.get().getRegistration());

    }

    @Test
    public void testfindByEmail(){

        entityManager.persist(profile);

        Optional<Profile> p = profileRepository.findByEmail(profile.getEmail());

        assertTrue(p.isPresent());
        assertEquals(profile.getProfileId(), p.get().getProfileId());
        assertEquals(profile.getEmail(), p.get().getEmail());
        assertEquals(profile.getFirstName(), p.get().getFirstName());
        assertEquals(profile.getLastName(), p.get().getLastName());
        assertEquals(profile.getTown(), p.get().getTown());
        assertThat(profile.getBirthday()).isEqualTo(p.get().getBirthday());
        assertThat(profile.getRegistration()).isEqualTo(p.get().getRegistration());

    }

    @Test
    public void testSave(){

        profileRepository.save(profile);

        Profile p = entityManager.find(Profile.class, profile.getProfileId());

        assertEquals(profile.getProfileId(), p.getProfileId());
        assertEquals(profile.getEmail(), p.getEmail());
        assertEquals(profile.getFirstName(), p.getFirstName());
        assertEquals(profile.getLastName(), p.getLastName());
        assertEquals(profile.getTown(), p.getTown());
        assertThat(profile.getBirthday()).isEqualTo(p.getBirthday());
        assertThat(profile.getRegistration()).isEqualTo(p.getRegistration());
    }
}
