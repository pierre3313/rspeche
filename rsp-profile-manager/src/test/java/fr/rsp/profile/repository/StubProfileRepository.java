package fr.rsp.profile.repository;

import fr.rsp.profile.model.Profile;
import fr.rsp.profile.model.ProfileForTest;

import java.util.Optional;

public class StubProfileRepository implements ProfileRepository {

    Profile profile;

    public StubProfileRepository(){

        this.profile = ProfileForTest.getProfileForTest();
    }

    @Override
    public Optional<Profile> findById(String s){

        if(!profile.getProfileId().equals(s))
            return Optional.empty();

        return Optional.of(profile);
    }

    @Override
    public Optional<Profile> findByEmail(String s) {

        if(!profile.getEmail().equals(s))
            return Optional.empty();

        return Optional.of(profile);
    }

    @Override
    public Profile save(Profile profile){
        return profile;
    }

    @Override
    public <S extends Profile> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public Iterable<Profile> findAll() {
        return null;
    }

    @Override
    public Iterable<Profile> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(Profile profile) {

    }

    @Override
    public void deleteAll(Iterable<? extends Profile> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
