package fr.rsp.profile.service;

import fr.rsp.profile.exception.ProfileException;
import fr.rsp.profile.model.Profile;
import fr.rsp.profile.model.ProfileForTest;
import fr.rsp.profile.repository.StubProfileRepository;
import fr.rsp.profile.service.impl.ProfileServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.security.NoSuchAlgorithmException;

import static junit.framework.TestCase.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class ProfileServiceTest {

    ProfileService profileService;

    @Before
    public void setUp(){

        profileService = new ProfileServiceImpl(new StubProfileRepository());
    }

    @Test
    public void testFindProfileById(){

        Profile profileForTest = ProfileForTest.getProfileForTest();

        Profile profile = profileService.findProfileById(profileForTest.getProfileId());

        assertEquals(profileForTest.getProfileId(), profile.getProfileId());
        assertEquals(profileForTest.getEmail(), profile.getEmail());
        assertEquals(profileForTest.getFirstName(), profile.getFirstName());
        assertEquals(profileForTest.getLastName(), profile.getLastName());
        assertEquals(profileForTest.getBlog(), profile.getBlog());
        assertEquals(profileForTest.getTown(), profile.getTown());
        assertThat(profileForTest.getBirthday()).isEqualTo(profile.getBirthday());
        assertThat(profileForTest.getRegistration()).isEqualTo(profile.getRegistration());

    }

    @Test(expected=ProfileException.class)
    public void testFindProfileById_withNotProfileId(){

        profileService.findProfileById("HASHERROR");

    }

    @Test
    public void testFindProfileByEmail(){

        Profile profileForTest = ProfileForTest.getProfileForTest();

        Profile profile = profileService.findProfileByEmail(profileForTest.getEmail());

        assertEquals(profileForTest.getProfileId(), profile.getProfileId());
        assertEquals(profileForTest.getEmail(), profile.getEmail());
        assertEquals(profileForTest.getFirstName(), profile.getFirstName());
        assertEquals(profileForTest.getLastName(), profile.getLastName());
        assertEquals(profileForTest.getBlog(), profile.getBlog());
        assertEquals(profileForTest.getTown(), profile.getTown());
        assertThat(profileForTest.getBirthday()).isEqualTo(profile.getBirthday());
        assertThat(profileForTest.getRegistration()).isEqualTo(profile.getRegistration());

    }

    @Test(expected=ProfileException.class)
    public void testFindProfileById_withNotValidEmail(){

        profileService.findProfileByEmail("error@gmail.com");

    }

    @Test
    public void testSaveProfile() throws NoSuchAlgorithmException {

        Profile profileForTest = ProfileForTest.getProfileForTest();

        Profile profileEnregister = profileService.saveProfile(profileForTest);

        assertEquals(profileService.generateProfileId_withMd5(profileForTest.getEmail()), profileEnregister.getProfileId());
        assertEquals(profileForTest.getEmail(), profileEnregister.getEmail());
        assertEquals(profileForTest.getFirstName(), profileEnregister.getFirstName());
        assertEquals(profileForTest.getLastName(), profileEnregister.getLastName());
        assertEquals(profileForTest.getBlog(), profileEnregister.getBlog());
        assertEquals(profileForTest.getTown(), profileEnregister.getTown());
        assertThat(profileForTest.getBirthday()).isEqualTo(profileEnregister.getBirthday());
        assertThat(profileForTest.getRegistration()).isEqualTo(profileEnregister.getRegistration());

    }

}
