CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting


 INTRODUCTION
 ------------

 REQUIREMENTS
 ------------

 RECOMMENDED MODULES
 -------------------

 INSTALLATION
 ------------

    * Docker Mysql =>
            - Pour télécharger l'image docker mysql :
                        docker pull mysql/mysql-server:5.7.21
            - Pour créer le container mysql :
                        docker run --name=mysql-rsp -p 3306:3306 -d mysql/mysql-server:5.7.21
            - Pour supprimer le container :
                        docker rm mysql-rsp
            - Pour démarrer/stopper le container :
                        docker start mysql-rsp
                        docker stop mysql-rsp
            - Pour afficher les logs :
                        docker logs mysql-rsp (le MDP est affiché au premier démarrage)
                            ex : [Entrypoint] GENERATED ROOT PASSWORD: (4r1ej3DtoHexuk4DAN20NocfIN


 CONFIGURATION
 -------------
    * Docker Mysql =>
            - Se connecter :
                    docker exec -it mysql-rsp mysql -uroot -p
            - Changer le MDP :
                    ALTER USER 'root'@'localhost' IDENTIFIED BY 'mysqlrsp';

 TROUBLESHOOTING
 ---------------