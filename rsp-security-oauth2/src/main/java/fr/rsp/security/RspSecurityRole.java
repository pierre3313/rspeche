package fr.rsp.security;

import fr.rsp.security.model.Authority;

public class RspSecurityRole {

    public static final Authority RSP_USER_CREATE = new Authority(1L, "RSP_USER_CREATE");
    public static final Authority RSP_USER_READ = new Authority(2L, "RSP_USER_READ");
    public static final Authority RSP_USER_UPDATE = new Authority(3L, "RSP_USER_UPDATE");
    public static final Authority RSP_USER_DELETE = new Authority(4L, "RSP_USER_DELETE");

    public static final Authority RSP_ADMIN_CREATE = new Authority(5L, "RSP_ADMIN_CREATE");
    public static final Authority RSP_ADMIN_READ = new Authority(6L, "RSP_ADMIN_READ");
    public static final Authority RSP_ADMIN_UPDATE = new Authority(7L, "RSP_ADMIN_UPDATE");
    public static final Authority RSP_ADMIN_DELETE = new Authority(8L, "RSP_ADMIN_DELETE");

}
