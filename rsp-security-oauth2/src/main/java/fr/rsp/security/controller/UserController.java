package fr.rsp.security.controller;

import fr.rsp.security.controller.dto.UserDto;
import fr.rsp.security.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
public interface UserController {

    public List<User> listUser();

    public Principal user(Principal principal);

    public ResponseEntity<UserDto> registerNewUser(UserDto userDto);
}
