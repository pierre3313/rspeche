package fr.rsp.security.controller.impl;

import fr.rsp.security.controller.UserController;
import fr.rsp.security.controller.dto.UserDto;
import fr.rsp.security.controller.mapping.MapUserDto;
import fr.rsp.security.exception.EmailExistsException;
import fr.rsp.security.model.User;
import fr.rsp.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserControllerImpl implements UserController {

    @Autowired
    private UserService userService;

    @Override
    @GetMapping("/")
    @PreAuthorize("hasAuthority('RSP_ADMIN_READ')")
    public List<User> listUser(){
        return userService.findAll();
    }

    @Override
    @GetMapping("/me")
    @PreAuthorize("hasAuthority('RSP_USER_READ') or hasAuthority('RSP_ADMIN_READ')")
    public Principal user(Principal principal) {
        return principal;
    }

    @Override
    @PostMapping("/")
    public ResponseEntity<UserDto> registerNewUser(@RequestBody UserDto userDto) {

        try {
            User user = userService.registerNewUserAccount(userDto);
            userDto = MapUserDto.mapUserToUserDto(user);
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        } catch (EmailExistsException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}
