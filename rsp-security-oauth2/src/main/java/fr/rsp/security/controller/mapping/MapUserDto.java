package fr.rsp.security.controller.mapping;

import fr.rsp.security.controller.dto.UserDto;
import fr.rsp.security.model.User;

public class MapUserDto {

    private MapUserDto(){}

    public static UserDto mapUserToUserDto(User user){

        UserDto userDto = new UserDto();
        userDto.setEmail(user.getUsername());

        return userDto;
    }

}
