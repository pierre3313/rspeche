package fr.rsp.security.service;

import fr.rsp.security.controller.dto.UserDto;
import fr.rsp.security.exception.EmailExistsException;
import fr.rsp.security.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserService {

    public UserDetails loadUserByUsername(String username);

    public List<User> findAll();

    public User registerNewUserAccount(UserDto userDto) throws EmailExistsException;
}
