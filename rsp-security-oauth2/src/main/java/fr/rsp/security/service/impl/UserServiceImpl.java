package fr.rsp.security.service.impl;

import fr.rsp.security.RspSecurityRole;
import fr.rsp.security.controller.dto.UserDto;
import fr.rsp.security.exception.EmailExistsException;
import fr.rsp.security.model.Authority;
import fr.rsp.security.model.User;
import fr.rsp.security.repository.UserRepository;
import fr.rsp.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service("userDetailsService")
public class UserServiceImpl implements UserDetailsService, UserService {

    private UserRepository userRepository;

    private PasswordEncoder userPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           PasswordEncoder userPasswordEncoder){
        this.userRepository = userRepository;
        this.userPasswordEncoder = userPasswordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {

        User user = userRepository.findOneByUsername(username);
        if (user != null) {
            return user;
        }
        return null;
    }

    @Override
    public User registerNewUserAccount(UserDto userDto) throws EmailExistsException {

        if (loadUserByUsername(userDto.getEmail())  != null ) {
            throw new EmailExistsException(
                    "Un compte a déjà été créé avec l'email : " + userDto.getEmail());
        }
        User user = new User();

        user.setUsername(userDto.getEmail());
        user.setPassword(userPasswordEncoder.encode(userDto.getPassword()));

        Collection<Authority> authorities = new ArrayList<>();
        authorities.add(RspSecurityRole.RSP_USER_CREATE);
        authorities.add(RspSecurityRole.RSP_USER_DELETE);
        authorities.add(RspSecurityRole.RSP_USER_READ);
        authorities.add(RspSecurityRole.RSP_USER_UPDATE);

        user.setAuthorities(authorities);

        return userRepository.save(user);
    }

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        userRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }
}
