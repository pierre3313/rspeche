package fr.rsp.security.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.rsp.security.Oauth2ServiceApplication;
import fr.rsp.security.Oauth2ServiceApplicationTest;
import fr.rsp.security.RspSecurityRole;
import fr.rsp.security.controller.dto.UserDto;
import fr.rsp.security.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.Base64Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Oauth2ServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UserControllerTest {

    private static String bearer;

    @Autowired
    MockMvc mvc;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() throws Exception {

        MockHttpServletResponse response = mvc
                .perform(post("/oauth/token")
                        .header("Authorization", "Basic "
                                + new String(Base64Utils.encode(("rsp-client:rsp-secret")
                                .getBytes())))
                        .param("username", "admin@gmail.com")
                        .param("password", "password")
                        .param("grant_type", "password"))
                .andReturn().getResponse();

        ObjectMapper mapper = new ObjectMapper();
        bearer =  mapper
                .readValue(response.getContentAsByteArray(), OAuthToken.class)
                .accessToken;

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class OAuthToken {
        @JsonProperty("access_token")
        public String accessToken;
    }

    @Test
    public void testUser() throws Exception {

        mvc.perform(get("/user/me")
                .header("Authorization", "Bearer " + bearer)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testRegisterNewUser() throws Exception {

        UserDto userDto = new UserDto();
        userDto.setEmail("test@mail.com");
        userDto.setPassword("password");

        ObjectMapper mapper = new ObjectMapper();

        MvcResult mvcResult = mvc.perform(post("/user/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + bearer)
                .content(mapper.writeValueAsString(userDto)))
                .andExpect(status().isOk()).andReturn();

        //Test du retour du service REST
        UserDto uDtoRest = mapper.readValue(mvcResult.getResponse().getContentAsString(), UserDto.class);
        assertEquals(uDtoRest.getEmail(), userDto.getEmail());

        //Test persistance en base
        UserDetails uBdd = userService.loadUserByUsername("test@mail.com");
        assertEquals(uBdd.getUsername(), userDto.getEmail());
        assertTrue(uBdd.getAuthorities().stream().anyMatch(o -> ((GrantedAuthority) o).getAuthority().equals(RspSecurityRole.RSP_USER_CREATE.getName())));
        assertTrue(uBdd.getAuthorities().stream().anyMatch(o -> ((GrantedAuthority) o).getAuthority().equals(RspSecurityRole.RSP_USER_DELETE.getName())));
        assertTrue(uBdd.getAuthorities().stream().anyMatch(o -> ((GrantedAuthority) o).getAuthority().equals(RspSecurityRole.RSP_USER_READ.getName())));
        assertTrue(uBdd.getAuthorities().stream().anyMatch(o -> ((GrantedAuthority) o).getAuthority().equals(RspSecurityRole.RSP_USER_UPDATE.getName())));
    }

    @Test
    public void testRegisterNewUser_withExistAccount() throws Exception{

        UserDto userDto = new UserDto();
        userDto.setEmail("admin@gmail.com");
        userDto.setPassword("password");

        ObjectMapper mapper = new ObjectMapper();

        MvcResult mvcResult = mvc.perform(post("/user/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + bearer)
                .content(mapper.writeValueAsString(userDto)))
                .andExpect(status().is(HttpStatus.CONFLICT.value())).andReturn();
    }

}
