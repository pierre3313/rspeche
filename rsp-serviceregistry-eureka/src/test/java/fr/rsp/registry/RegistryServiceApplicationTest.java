package fr.rsp.registry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RegistryServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegistryServiceApplicationTest {

    @Test
    public void contextLoads() {
    }

    @Test
    public void contextLoadsPlus(){
        RegistryServiceApplication.main(new String[]{
        });
    }
}
