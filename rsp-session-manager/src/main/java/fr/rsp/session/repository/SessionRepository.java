package fr.rsp.session.repository;

import fr.rsp.session.model.Session;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SessionRepository extends MongoRepository<Session, String> {

    public Session findBySessionId(String sessionId);

    public List<Session> findByProfileId(String profileId);

}
