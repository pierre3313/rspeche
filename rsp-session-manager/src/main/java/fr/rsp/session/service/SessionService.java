package fr.rsp.session.service;

import fr.rsp.session.model.Session;

import java.util.List;

public interface SessionService {

    public Session findBySessionId(String sessionId);

    public List<Session> findByProfileId(String profileId);

    public void saveSession(Session session);

    public void deleteSession(Session session);

}
