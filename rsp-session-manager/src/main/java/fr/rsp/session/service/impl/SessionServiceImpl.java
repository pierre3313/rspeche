package fr.rsp.session.service.impl;

import fr.rsp.session.model.Session;
import fr.rsp.session.repository.SessionRepository;
import fr.rsp.session.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.logging.Logger;

public class SessionServiceImpl implements SessionService {

    private static final Logger logger = Logger.getLogger(SessionServiceImpl.class.getName());

    private SessionRepository sessionRepository;

    @Autowired
    public SessionServiceImpl(SessionRepository sessionRepository){
        this.sessionRepository = sessionRepository;
    }

    @Override
    public Session findBySessionId(String sessionId) {
        return this.sessionRepository.findBySessionId(sessionId);
    }

    @Override
    public List<Session> findByProfileId(String profileId) {
        return this.sessionRepository.findByProfileId(profileId);
    }

    @Override
    public void saveSession(Session session) {
        this.sessionRepository.save(session);
    }

    @Override
    public void deleteSession(Session session) {
        this.sessionRepository.delete(session);
    }

}
