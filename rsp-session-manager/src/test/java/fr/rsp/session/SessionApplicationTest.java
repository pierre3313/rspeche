package fr.rsp.session;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SessionApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SessionApplicationTest {

    @Test
    public void contextLoads() {

    }

    @Test
    public void contextLoadsPlus(){
        SessionApplication.main(new String[]{
                "--spring.main.web-environment=false"
        });
    }

}
