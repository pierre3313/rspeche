package fr.rsp.session.repository;

import fr.rsp.session.model.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@DataMongoTest
public class SessionRepositoryTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private SessionRepository sessionRepository;

    private Session sessionCompare;

    @Before
    public void setUp(){

        Session session = new Session();

        session.setSessionId("IDTEST123456");
        session.setName("Session pour test");
        session.setProfileId("PROFILEIDTEST");

        LocalDateTime ldt = LocalDateTime.now();
        session.setSessionStart(Date.from(ldt.withDayOfMonth(25).withYear(2018).withMonth(12).toInstant(ZoneOffset.UTC)));
        session.setSessionEnd(Date.from(ldt.withDayOfMonth(26).withYear(2018).withMonth(12).toInstant(ZoneOffset.UTC)));

        mongoTemplate.save(session);

        sessionCompare = session;
    }

    @Test
    public void testFindBySessionId(){

        Session session = sessionRepository.findBySessionId("IDTEST123456");

        assertEquals(sessionCompare.getName(), session.getName());
        assertEquals(sessionCompare.getProfileId(), session.getProfileId());
        assertEquals(sessionCompare.getSessionStart(), session.getSessionStart());
        assertEquals(sessionCompare.getSessionEnd(), session.getSessionEnd());
        assertEquals(sessionCompare.getSessionId(), session.getSessionId());
    }

    @Test
    public void testFindByProfileId(){

        List<Session> lSessions = sessionRepository.findByProfileId("PROFILEIDTEST");

        assertEquals(sessionCompare.getName(), lSessions.get(0).getName());
        assertEquals(sessionCompare.getProfileId(), lSessions.get(0).getProfileId());
        assertEquals(sessionCompare.getSessionStart(), lSessions.get(0).getSessionStart());
        assertEquals(sessionCompare.getSessionEnd(), lSessions.get(0).getSessionEnd());
        assertEquals(sessionCompare.getSessionId(), lSessions.get(0).getSessionId());
    }

    @Test
    public void testSave(){

        Session sessionSave = new Session();

        sessionSave.setSessionId("IDTEST654321");
        sessionSave.setName("Session pour test save");
        sessionSave.setProfileId("PROFILEIDTESTSAVE");

        LocalDateTime ldt = LocalDateTime.now();
        sessionSave.setSessionStart(Date.from(ldt.withDayOfMonth(25).withYear(2018).withMonth(12).toInstant(ZoneOffset.UTC)));
        sessionSave.setSessionEnd(Date.from(ldt.withDayOfMonth(26).withYear(2018).withMonth(12).toInstant(ZoneOffset.UTC)));

        sessionRepository.save(sessionSave);

        Query query = new Query();
        query.addCriteria(Criteria.where("sessionId").is("IDTEST654321"));

        Session session = mongoTemplate.findOne(query, Session.class);

        assertEquals(sessionSave.getName(), session.getName());
        assertEquals(sessionSave.getProfileId(), session.getProfileId());
        assertEquals(sessionSave.getSessionStart(), session.getSessionStart());
        assertEquals(sessionSave.getSessionEnd(), session.getSessionEnd());
        assertEquals(sessionSave.getSessionId(), session.getSessionId());
    }
}
