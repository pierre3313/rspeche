# RspViewAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Links

### MDBootstrap
https://mdbootstrap.com/angular/5min-quickstart/

https://medium.com/@tomastrajan/how-to-build-responsive-layouts-with-bootstrap-4-and-angular-6-cfbb108d797b
https://openclassrooms.com/courses/4668271-developpez-des-applications-web-avec-angular/5087065-structurez-avec-les-components
https://github.com/ngx-rocket/starter-kit
https://angular.io/tutorial/toh-pt2#update-the-details-template
https://openclassrooms.com/courses/1885491-prenez-en-main-bootstrap/1887143-les-composants-integres

http://jasonwatmore.com/post/2018/05/23/angular-6-jwt-authentication-example-tutorial

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
