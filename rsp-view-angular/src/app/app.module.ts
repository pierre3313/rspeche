import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { HomeComponent } from './home/home.component';

import { AuthService } from './core/authentication/auth.service';
import { AuthGuard } from './core/guard/auth-guard.service';
import { ContentMainComponent } from './home/content-main/content-main.component';
import { FooterHomeComponent } from './home/footer-home/footer-home.component';
import { NavBarComponent } from './home/nav-bar/nav-bar.component';
import { AccountComponent } from './home/content-main/account/account.component';
import { HttpAuthorizationInterceptor } from './core/interceptor/httpAuthorization.interceptor';
import { ProfileCardsComponent } from './home/content-main/profile-cards/profile-cards.component';
import { ProfileService } from './core/profile/profile.service';
import { DateFormatPipe } from './shared/pipe/DateFormat.pipe';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', pathMatch: 'full', redirectTo: 'app'},
  { path: 'app', canActivate: [AuthGuard], component: HomeComponent, children:
    [
      {path: '', component: ContentMainComponent, outlet: 'homemain' },
      {path: 'account', component: AccountComponent, outlet: 'homemain'}
    ]
  },
  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    FourOhFourComponent,
    ContentMainComponent,
    FooterHomeComponent,
    NavBarComponent,
    AccountComponent,
    ProfileCardsComponent,
    DateFormatPipe
  ],
  imports: [    
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot()
  ],
  providers: [
    MDBSpinningPreloader,
    Title,
    AuthService,
    AuthGuard,
    ProfileService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpAuthorizationInterceptor, multi: true },
  ],  
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ]
})

export class AppModule { }
