import { environment } from "src/environments/environment";

export const oauthConfig = {

    authorization: 'Basic ' + btoa("rsp-client:" + environment.oauthRspClientSecret)

}