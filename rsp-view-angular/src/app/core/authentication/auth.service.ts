import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { oauthConfig } from './auth.config';
import { Profile } from '../../model/Profile.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Authorization': oauthConfig.authorization
  })
};

interface Token {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string
}

@Injectable()
export class AuthService {

    constructor(private http: HttpClient) { }

    isAuth = false;
    user: Profile;
    
  
    signIn(username: string, password: string) {

      let postData = new FormData();
      postData.append('username', username);
      postData.append('password', password);
      postData.append('grant_type','password');

      return new Promise(
        (resolve, reject) => {

          this.http.post<Token>(environment.tokenUri, 
          postData,
          httpOptions)
          .subscribe(
            data => {
                console.log("POST Request login is successful ", data);
                localStorage.setItem('rsptoken', JSON.stringify(data));

                this.isAuth = true;
                resolve(true);
            },
            error => {
              console.log("Error", error);
              reject()
            }
          );
        }
      );
    }
  
    signOut() {

      localStorage.removeItem('rsptoken');

      this.isAuth = false;
    }
  }