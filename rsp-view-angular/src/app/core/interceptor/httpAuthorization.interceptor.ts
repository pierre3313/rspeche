import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

/**
 * Intercepteur permettant de positionner le jeton d'authentification dans chaque requête
 * quand celui-ci éxiste
 */
@Injectable()
export class HttpAuthorizationInterceptor implements HttpInterceptor {
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        let rsptoken = JSON.parse(localStorage.getItem('rsptoken'));
        if (rsptoken && rsptoken.access_token) {
            request = request.clone({
                setHeaders: { 
                    Authorization: 'Bearer ' + rsptoken.access_token
                }
            });
        }

        return next.handle(request);
    }
}