import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Profile } from "src/app/model/Profile.model";

/**
 * Service utilisé pour dialoguer avec la resource Profile côté serveur
 */
@Injectable()
export class ProfileService {

    constructor(private http: HttpClient){}

    getProfile(){
        return this.http.get<Profile>(environment.profileServiceUrl + '/profile');
    }
}