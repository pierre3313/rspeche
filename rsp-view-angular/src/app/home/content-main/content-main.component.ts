import { Component, OnInit } from '@angular/core';
import OlMap from 'ol/map';
import OlXYZ from 'ol/source/xyz';
import OlTileLayer from 'ol/layer/tile';
import OlVectorLayer from 'ol/layer/vector';
import OlVectorSource from 'ol/source/vector';
import OlView from 'ol/view';
import OlProj from 'ol/proj';
import OlFeature from 'ol/feature';
import OlPoint from 'ol/geom/point';
import OlMultiPoint from 'ol/geom/multipoint';
import OlStyle from 'ol/style/style';
import OlIcon from 'ol/style/icon';
import OlStroke from 'ol/style/stroke';
import OlCircleStyle from 'ol/style/circle';
import OlFill from 'ol/style/fill';

@Component({
  selector: 'app-content-main',
  templateUrl: './content-main.component.html',
  styleUrls: ['./content-main.component.scss']
})
export class ContentMainComponent implements OnInit {

  map: OlMap;
  source: OlXYZ;
  layer: OlTileLayer;
  view: OlView;
  feature: OlFeature;
  vectorLayer: OlVectorLayer;

  public posts: Object[] = [
    {
      icon: 'fa fa-heart',
      color: 'teal-text',
      category: 'Lifestyle',
      title: 'This is title of the news',
      img: 'https://mdbootstrap.com/img/Photos/Others/img (38).jpg',
      content: `Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo
                minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor.`,
      by: 'Jessica Clark',
      date: '26/08/2016',
      number: 'First',
    },
    {
      icon: 'fa fa-plane',
      color: 'cyan-text',
      category: 'Travels',
      title: 'This is title of the news',
      img: 'https://mdbootstrap.com/img/Photos/Others/forest-sm.jpg',
      content: `At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
               voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati.`,
      by: 'Jessica Clark',
      date: '24/08/2016',
      number: 'Second',
    },
    {
      icon: 'fa fa-camera',
      color: 'brown-text',
      category: 'Photography',
      title: 'This is title of the news',
      img: 'https://mdbootstrap.com/img/Photos/Others/img (35).jpg',
      content: `Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.`,
      by: 'Jessica Clark',
      date: '21/08/2016',
      number: 'Third',
    },
    {
      icon: 'fa fa-heart',
      color: 'red-text',
      category: 'Lifestyle',
      title: 'This is title of the news',
      img: 'https://mdbootstrap.com/img/Photos/Others/img (39).jpg',
      content: `Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.`,
      by: 'Jessica Clark',
      date: '21/08/2016',
      number: 'Fourth',
    },
  ];

  constructor() { }

  ngOnInit() {

    var multiPoint = new OlMultiPoint([OlProj.fromLonLat([-0.5874382,44.7737666])]);
    multiPoint.appendPoint(new OlPoint(OlProj.fromLonLat([-1.085500,44.486626])));

    this.feature = new OlFeature({
      type: 'icon',
      //geometry: new OlPoint(OlProj.fromLonLat([-0.5874382,44.7737666]))
      geometry: multiPoint
    });   
    
    var styles = {
      'route': new OlStyle({
        stroke: new OlStroke({
          width: 6, color: [237, 212, 0, 0.8]
        })
      }),      
      'icon': new OlStyle({
        image: new OlIcon({
          anchor: [0.5, 1],
          src: 'assets/gps_PNG18.png'
        })
      }),
      'geoMarker': new OlStyle({
        image: new OlCircleStyle({
          radius: 7,
          snapToPixel: false,
          fill: new OlFill({color: 'black'}),
          stroke: new OlStroke({
            color: 'white', width: 2
          })
        })
      })
    };

    this.vectorLayer = new OlVectorLayer({
      source: new OlVectorSource({
        features: [this.feature]
      }),
      style: styles['icon']
    })

    //source dispo sur http://leaflet-extras.github.io/leaflet-providers/preview/
    this.source = new OlXYZ({
      url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
      //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'
    });

    this.layer = new OlTileLayer({
      source: this.source
    });

    this.view = new OlView({
      center: OlProj.fromLonLat([-0.5874382,44.7737666]),
      zoom: 9
    });

    this.map = new OlMap({
      target: 'map',
      layers: [this.layer, this.vectorLayer],
      view: this.view
    });
  }

}
