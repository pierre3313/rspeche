import { Component, OnInit, Input } from '@angular/core';
import { Profile } from 'src/app/model/Profile.model';
import { ProfileService } from 'src/app/core/profile/profile.service';

@Component({
  selector: 'app-profile-cards',
  templateUrl: './profile-cards.component.html',
  styleUrls: ['./profile-cards.component.scss']
})
export class ProfileCardsComponent implements OnInit {

  constructor(private profileService : ProfileService) { }

  @Input() profile: Profile;

  ngOnInit() {

    this.profileService.getProfile().subscribe(
      (data: Profile) => {
        this.profile = data;
        console.log('Profile ok : ' + data);
      },
      error => {
        console.log('Erreur à la récupération du profile');
      }
    )
  }

}
