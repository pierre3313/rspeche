import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../core/authentication/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements  OnInit, OnDestroy {


  constructor(private renderer: Renderer2,
    private authService : AuthService) { }

  ngOnInit() {
    this.renderer.addClass(document.body, "mdb-skin");
    this.renderer.addClass(document.body, "fixed-sn");
    this.renderer.addClass(document.body, "body-eee");
  }

  ngOnDestroy(): void {
    this.authService.signOut();

    this.renderer.removeClass(document.body, "mdb-skin");
    this.renderer.removeClass(document.body, "fixed-sn");
    this.renderer.removeClass(document.body, "body-eee");
  }

}
