import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/authentication/auth.service';
import { Router } from '@angular/router';

/**
 * Composant qui gère la navigation une fois l'utilisateur connecté
 */
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  /**
   * Constructeur du composant NavBar
   * 
   * @param authService
   * @param router 
   */
  constructor(private authService : AuthService,
    private router : Router) {

   }

  ngOnInit() {
  }

  signOut(){
    this.authService.signOut();

    this.router.navigate(['/login']);
  }

}
