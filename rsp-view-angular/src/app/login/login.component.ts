import { Renderer2, Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../core/authentication/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * Composant dédié à la page d'accueil et surtout à la partie Login
 * Le formulaire est géré avec le module ReactiveFormModule
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  authStatus: boolean;

  /**
   * Constructeur du composant Login.
   * 
   * @param formBuilder 
   * @param authService 
   * @param router 
   */
  constructor(private formBuilder: FormBuilder,
                private authService: AuthService, 
                private router : Router) {
    this.authStatus=true;
  }

  ngOnInit() {
    this.initForm();
  }

  /**
   *  Initialisation du formulaire et des validators
   */
  initForm(){
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnDestroy(): void {
  }

  /**
   * Activé au moment du submit du formulaire
   */
  onSignIn() {
    const formValue = this.loginForm.value;
    this.authService.signIn(formValue['username'], formValue['password']).then(
      () => {
        console.log('Sign in successful!');
        this.authStatus = true;
        this.router.navigate(['/']);
      }
    ).catch(
      () => {
        console.log('Sign in error!');
        this.authStatus = false;
      }
    );
  }
}
