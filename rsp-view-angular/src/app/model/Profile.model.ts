export class Profile {
    profileId: number;
    email: string;
    firstName: string;
    lastName: string; 
    birthday: Date;
    blog: string;
    registration: Date;
    town: string;
}