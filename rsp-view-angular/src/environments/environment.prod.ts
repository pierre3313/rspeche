/**
 * Fichier de constantes utilisés pour la production
 */
export const environment = {
  production: true,
  tokenUri: 'https://rspservice.psoi.fr/security/oauth/token',
  authorizationUri: 'https://rspservice.psoi.fr/security/oauth/authorize',
  userInfoUri: 'https://rspservice.psoi.fr/security/user/me',
  oauthRspClientSecret: 'rsp-secret',

  //Url pour le service profile
  profileServiceUrl: 'http://rspservice.psoi.fr/profile'
};
