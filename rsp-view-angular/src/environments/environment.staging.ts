/**
 * Fichier de constantes utilisés en local
 */
export const environment = {
    production: false,
    tokenUri: 'http://localhost:8105/security/oauth/token',
    authorizationUri: 'http://localhost:8105/security/oauth/authorize',
    userInfoUri: 'http://localhost:8105/security/user/me',
    oauthRspClientSecret: 'rsp-secret',

    //Url pour le service profile
    profileServiceUrl: 'http://localhost:8105/profile'
  };