// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  //Urls utilisées pour l'authentification
  tokenUri: 'https://rspservice.psoi.fr/security/oauth/token',
  authorizationUri: 'https://rspservice.psoi.fr/security/oauth/authorize',
  userInfoUri: 'https://rspservice.psoi.fr/security/user/me',
  oauthRspClientSecret: 'rsp-secret',

  //Url pour le service profile
  profileServiceUrl: 'http://rspservice.psoi.fr/profile'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
