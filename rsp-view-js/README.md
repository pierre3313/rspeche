# Project Title

One Paragraph of project description goes here

## Pour commencer

### Prérequis

What things you need to install the software and how to install them

```
Give examples
```

### Installation

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Excécuter les tests

Explain how to run the automated tests for this system

## Déploiment

Add additional notes about how to deploy this on a live system

## Technologies

* [Spring boot](https://projects.spring.io/spring-boot/) - Frame
* [Maven](https://maven.apache.org/) - Manager de dépendance java
* [Docker](https://www.docker.com/) - Container

## Version

RSPeche utilise bitbucket (https://bitbucket.org/) pour la gestion de ses sources et de ces versions. Les versions sont visibles sous tags sur le répository https://pierre3313@bitbucket.org/pierre3313/rspeche.git

## Auteur

* **Pierre-Edouard Soupault** - *Version initiale*

## Copyright 

Ce programme est la propriété de Pierre-Edouard Soupault.