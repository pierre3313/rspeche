(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\components\\messages\\_breakpoints.js":[function(require,module,exports){
(function ($) {
    "use strict";

    $(window).bind('enterBreakpoint320', function () {
        var img = $('.messages-list .panel ul img');
        $('.messages-list .panel ul').width(img.first().width() * img.length);
    });

    $(window).bind('exitBreakpoint320', function () {
        $('.messages-list .panel ul').width('auto');
    });

})(jQuery);

},{}],"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\components\\messages\\_nicescroll.js":[function(require,module,exports){
(function ($) {
    "use strict";

    var nice = $('.messages-list .panel').niceScroll({cursorborder: 0, cursorcolor: "#25ad9f", zindex: 1});

    var _super = nice.getContentSize;

    nice.getContentSize = function () {
        var page = _super.call(nice);
        page.h = nice.win.height();
        return page;
    };

})(jQuery);
},{}],"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\components\\messages\\main.js":[function(require,module,exports){
require('./_breakpoints');
require('./_nicescroll');
},{"./_breakpoints":"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\components\\messages\\_breakpoints.js","./_nicescroll":"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\components\\messages\\_nicescroll.js"}],"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\pages\\users.js":[function(require,module,exports){
(function ($) {
    "use strict";

    $('#users-filter-select').on('change', function () {
        if (this.value === 'name') {
            $('#user-first').removeClass('hidden');
            $('#user-search-name').removeClass('hidden');
        } else {
            $('#user-first').addClass('hidden');
            $('#user-search-name').addClass('hidden');
        }
        if (this.value === 'friends') {
            $('.select-friends').removeClass('hidden');

        } else {
            $('.select-friends').addClass('hidden');
        }
        if (this.value === 'name') {
            $('.search-name').removeClass('hidden');

        } else {
            $('.search-name').addClass('hidden');
        }
    });

})(jQuery);

},{}],"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\themes\\social-1\\main.js":[function(require,module,exports){
// Users
require('../../pages/users');

// Messages
require('../../components/messages/main');
},{"../../components/messages/main":"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\components\\messages\\main.js","../../pages/users":"D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\pages\\users.js"}]},{},["D:\\Pro\\Projets\\git\\rspeche\\rsp-view-js\\src\\js\\themes\\social-1\\main.js"]);
