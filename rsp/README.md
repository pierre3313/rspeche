# RSP

Projet Parent RSPeche

## Pour commencer

### Prérequis

What things you need to install the software and how to install them

```
Give examples
```

## Maven

``` 
mvn versions:set -DnewVersion=1.0.0
``` 

## Mysql et Docker

### Installation

* Pour télécharger l'image docker mysql :
    ``` 
    docker pull mysql/mysql-server:5.7.21
    ```
* Pour créer le container mysql :
    ```
    docker run --name=mysql-rsp -p 3306:3306 -d mysql/mysql-server:5.7.21
    ```
* Pour supprimer le container :
    ```
    docker rm mysql-rsp
    ```
* Pour démarrer/stopper le container :
    ```
    docker start mysql-rsp
    docker stop mysql-rsp
    ```
* Pour afficher les logs :
    ```
    docker logs mysql-rsp (le MDP est affiché au premier démarrage)
        ex : [Entrypoint] GENERATED ROOT PASSWORD: (4r1ej3DtoHexuk4DAN20NocfIN
    ```

### Configuration

* Se connecter :
    ```
    docker exec -it mysql-rsp mysql -uroot -p
    ```
* Changer le MDP :
    ```
    ALTER USER 'root'@'localhost' IDENTIFIED BY 'mysqlrsp';
    ```

## Technologies

* [Spring boot](https://projects.spring.io/spring-boot/) - Frame
* [Maven](https://maven.apache.org/) - Manager de dépendance java
* [Docker](https://www.docker.com/) - Container

## Version

RSPeche utilise bitbucket (https://bitbucket.org/) pour la gestion de ses sources et de ces versions. Les versions sont visibles sous tags sur le répository https://pierre3313@bitbucket.org/pierre3313/rspeche.git

## Auteur

* **Pierre-Edouard Soupault** - *Version initiale*

## Copyright 

Ce programme est la propriété de Pierre-Edouard Soupault.