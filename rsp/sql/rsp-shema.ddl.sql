CREATE DATABASE  rsp;

CREATE USER 'rsp'@'%' IDENTIFIED BY 'rsp';
CREATE USER 'rsp'@'localhost' IDENTIFIED BY 'rsp';
GRANT ALL PRIVILEGES ON rsp.* TO 'rsp'@'%';
GRANT ALL PRIVILEGES ON rsp.* TO 'rsp'@'localhost';